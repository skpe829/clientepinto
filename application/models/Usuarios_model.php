<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Usuarios_model extends CI_Model{	
		
		function __construct(){
			parent::__construct();
			$this->load->database();
		}

		function getUsuario($data){		
			$sql = "SELECT ced_ruc, email, permiso, nombre, apellido, username FROM tarjusers WHERE username = ? AND password = ?;";
			$rs = $this->db->query($sql,$data);
			if($rs) return $rs;
			else return false;
		}


	}
?>