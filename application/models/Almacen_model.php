<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Almacen_model extends CI_Model{	
		
		function __construct(){
			parent::__construct();
			$this->load->database();
		}

		function getAlmacenByIp($data){
			$sql = "SELECT * FROM stores7:info_servidores WHERE ip = ? AND ? IN (maq01,maq02) AND server_estado = 'A'";
			$rs = $this->db->query($sql,$data);
			if($rs) return $rs;
			else return false;
		}

		function getPromoAlmacen($data){
			$sql = "SELECT * FROM tarj03_config WHERE cod_alm = ? AND Today BETWEEN fec_ini AND fec_fin";
			$rs = $this->db->query($sql, $data);
			if($rs){
				if($rs->num_rows()>0){
					return $rs->Result();	
				}else{
					$sql = "SELECT * FROM tarj03_config WHERE cod_alm = '00'";
					$rs = $this->db->query($sql);
					return $rs->Result();
				}
			}else{
				return false;
			}
		}

		function getAlmacenes(){
			$sql = "SELECT * FROM stores7:info_servidores WHERE server_estado = 'A' AND server_tipo IN ('F','P') ORDER BY cod_alm ASC";
			$rs = $this->db->query($sql);
			if($rs) return $rs;
			else return false;
		}

	}
?>