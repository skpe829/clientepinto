<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Clientes_model extends CI_Model{	
		
		private $DB_ibm;
		private $DB_xime;

		function __construct(){
			parent::__construct();
			$this->DB_ibm = $this->load->database('default',TRUE);
			$this->DB_xime = $this->load->database('xime',TRUE);
			ini_set('max_execution_time', -1);
		}

		function getClientesPinto($data){
			$condicion = "";	
			if($data <> '00'){
				$condicion = "AND b.cod_alm = '".$data."'";
			}

			$sql = "SELECT a.ced_ruc[1,10]
					FROM mac03 a
					LEFT JOIN cliente b ON a.ced_ruc[1,10] = b.ced_ruc[1,10] AND b.cod_tar IS NULL
					WHERE a.ced_ruc <> '9999999999'
					AND a.cod_mov IN ('02','41')
					AND a.sta_anl > 0 AND a.sta_anl <> 9
					AND a.fec_mov >= Today - 90
					AND b.ced_ruc IS NOT NULL
					AND a.cod_alm[1] <> 'T'
					GROUP BY 1  
					HAVING SUM(CASE a.cod_mov WHEN '41' THEN (a.val_fct + a.val_iva) * -1 ELSE (a.val_fct + a.val_iva)  END) > 200
					INTO TEMP tt1; ";

					$sql2 = "SELECT TRIM(a.ced_ruc) ced_ruc, c.cod_alm, alm.nom_alm FROM tt1 a
					LEFT JOIN mac03 b ON b.ced_ruc[1,10] = a.ced_ruc[1,10] AND b.fec_mov >= Today - 90 AND b.cod_mov IN ('02','41') AND b.sta_anl > 0 AND b.sta_anl <> 9 AND b.cod_alm[1] <> 'T'
					LEFT JOIN integra c ON b.cod_mov = c.cod_mov AND b.num_mov = c.num_mov AND b.fec_mov = c.fec_mov AND b.cod_bdg = c.cod_bdg AND b.cod_alm = c.cod_alm 
					LEFT JOIN tbalm alm ON c.cod_alm = alm.cod_alm
					WHERE c.fecha1 = (
					    SELECT Max(e.fecha1) FROM mac03 d
					    LEFT JOIN integra e ON d.cod_mov = e.cod_mov AND d.num_mov = e.num_mov AND d.fec_mov = e.fec_mov AND d.cod_bdg = e.cod_bdg AND d.cod_alm = e.cod_alm
					    WHERE a.ced_ruc[1,10] = d.ced_ruc[1,10] AND d.fec_mov >= Today - 90 AND d.cod_mov IN ('02','41') AND d.sta_anl > 0 AND d.sta_anl <> 9 AND d.cod_alm[1] <> 'T' 
					)".$condicion;

			$this->DB_xime->query($sql);

			$rs = $this->DB_xime->query($sql2);

			if($rs) return $rs;
			else return false;
		}

		function getClientesPintoPromocion($data){
			$sql = "SELECT a.ced_ruc[1,10], num_mov,
					SUM(CASE a.cod_mov WHEN '41' THEN (a.val_fct + a.val_iva) * -1 ELSE (a.val_fct + a.val_iva)  END) fact
					FROM mac03 a
					LEFT JOIN cliente b ON a.ced_ruc[1,10] = b.ced_ruc[1,10] AND b.cod_tar IS NULL
					WHERE a.ced_ruc <> '9999999999'
					AND a.cod_mov IN ('02','41')
					AND a.sta_anl > 0 AND a.sta_anl <> 9
					--AND a.fec_mov >= Today - 10
					AND a.fec_mov BETWEEN (? AND ?)
					AND b.ced_ruc IS NOT NULL
					AND a.cod_alm = ?
					GROUP BY 1, 2
					HAVING SUM(CASE a.cod_mov WHEN '41' THEN (a.val_fct + a.val_iva) * -1 ELSE (a.val_fct + a.val_iva)  END) > ?";
			$rs = $this->DB_ibm->query($sql);

			if($rs) return $rs;
			else return false;
		}

		function getInfoClientes($data){
			$sql ="SELECT (TRIM(nvl(nombres,''))||' '||TRIM(nvl(apellidos,''))) nombres, TRIM(email) email, TRIM(nvl(sexo,'')) sexo, TRIM(nvl(fec_nacim,'')) fec_nac, nvl(telef,'') telef, nvl(telef2,'') telef2, (TRIM(nvl(dir_principal,''))||' '||TRIM(nvl(dir_secundaria,''))) dir FROM cliente WHERE ced_ruc[1,10] = ? AND cod_tar IS NULL";
			$rs = $this->DB_xime->query($sql,$data);
			if($rs) return $rs->Row();
			else return false;
		}

		function setClientesPinto($clientes){

			$this->DB_xime->trans_begin();

			foreach ($clientes as $key => $data) {
				$sql = "SELECT * FROM tar03 WHERE ced_ruc = '".$data['cedula']."';";
				$rs = $this->DB_xime->query($sql);
				$tar="";
				if($rs){
					if($rs->num_rows()==0){
						$tar03 = "INSERT INTO tar03 (cod_tar,nombre,direc,telef,tipo_doc,ced_ruc,fec_nacim,sexo,cod_ciu,fec_caduc,fec_ingreso,estado,telef2,fec_modif,cod_alm) 
								VALUES('".$data['cedula']."','".$data['nombres']."','".$data['dir']."','".$data['telef']."','C','".$data['cedula']."','".$data['fec_nac']."','".$data['sexo']."',1,Today+3650,Today,'W','',Today,'".$data['fec_nac']."');";
						$tar06 = "INSERT INTO tarj06 (ced_ruc,nom_tar,email,estatus) 
								VALUES('".$data['cedula']."','".$data['nombres']."','".$data['email']."','W');";
					}
					$cliente = "UPDATE cliente SET cod_tar = '".$data['cedula']."' WHERE ced_ruc[1,10] = '".$data['cedula']."';";
				
					$this->DB_xime->query($tar03);
					$this->DB_xime->query($tar06);
					$this->DB_xime->query($cliente);
				}
			}

			if ($this->DB_xime->trans_status() === FALSE){
			    $this->DB_xime->trans_rollback();
			    return array(true, "Sucedio un error mientras se insertaba en la BD, Intente nuevamente");
			}
			else{
				$this->DB_xime->trans_commit();
				return array(true, "El Almacen fue registrado Exitosamente");
			}
			
		}
	}





/*
$condicion = " AND 
						(SELECT DISTINCT b.cod_alm
						FROM mac03 b, integra c 
						WHERE b.ced_ruc[1,10] = a.ced_ruc[1,10]
						AND b.cod_mov = '02'
						AND b.num_mov = c.num_mov 
						AND b.cod_mov = c.cod_mov 
						AND b.fec_mov = c.fec_mov 
						AND b.cod_bdg = c.cod_bdg 
						AND b.cod_alm = c.cod_alm 
						AND c.fecha1 = (
							SELECT Max(fecha1) 
							FROM mac03 d, integra e 
							WHERE d.ced_ruc[1,10] = b.ced_ruc[1,10]
							AND d.cod_mov = '02' 
							AND d.fec_mov >= today - 90 
							AND d.num_mov = e.num_mov 
							AND d.cod_mov = e.cod_mov 
							AND d.fec_mov = e.fec_mov 
							AND d.cod_bdg = e.cod_bdg 
							AND d.cod_alm = e.cod_alm
						)) = '".$data."'";

$sql = "SELECT a.ced_ruc[1,10],
					(SELECT DISTINCT TRIM(x.nom_alm)
					FROM mac03 b, integra c, tbalm x
					WHERE b.ced_ruc[1,10] = a.ced_ruc[1,10]
					AND b.cod_mov = '02'
					AND b.num_mov = c.num_mov 
					AND b.cod_mov = c.cod_mov 
					AND b.fec_mov = c.fec_mov 
					AND b.cod_bdg = c.cod_bdg 
					AND b.cod_alm = c.cod_alm 
					AND a.cod_alm = x.cod_alm
					AND c.fecha1 = (
					    SELECT Max(fecha1) 
					    FROM mac03 d, integra e 
					    WHERE d.ced_ruc[1,10] = b.ced_ruc[1,10]
					    AND d.cod_mov = '02' 
					    AND d.fec_mov >= today - 90 
					    AND d.num_mov = e.num_mov 
					    AND d.cod_mov = e.cod_mov 
					    AND d.fec_mov = e.fec_mov 
					    AND d.cod_bdg = e.cod_bdg 
					    AND d.cod_alm = e.cod_alm
					)) nom_alm,
					SUM(CASE a.cod_mov WHEN '41' THEN (a.val_fct + a.val_iva) * -1 ELSE (a.val_fct + a.val_iva)  END) total
					FROM mac03 a
					LEFT JOIN cliente b ON a.ced_ruc[1,10] = b.ced_ruc[1,10]
					WHERE a.fec_mov >= Today - 90
					AND a.cod_mov IN ('02','41')
					AND a.sta_anl > 0 AND a.sta_anl <> 9
					AND (b.cod_tar IS NULL OR b.cod_tar = '' OR b.cod_tar = ' ')
					AND a.ced_ruc <> '9999999999'
					AND a.cod_alm[1] <> 'T'
					GROUP BY 1,2
					HAVING SUM(CASE a.cod_mov WHEN '41' THEN (a.val_fct + a.val_iva) * -1 ELSE (a.val_fct + a.val_iva)  END) > 200
					".$condicion."
					ORDER BY 3";
*/

?>
