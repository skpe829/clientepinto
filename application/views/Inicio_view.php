<div class="container">
	<div class="row justify-content-center">
		<div class="col-xl-10 col-lg-12 col-md-9">
			<div class="card o-hidden border-0 shadow-lg my-5">
				<div class="text-center">
					<h1 class="h4 text-gray-900 mb-4">Tarjetas Clientes Pinto</h1>
				</div>
				<form class="user" id="frmLogin">
					<div class="row">
						<div class="card-body p-5">
							<div class="form-group">
								<input type="text" class="form-control form-control-user" id="txtUsuario" name="txtUsuario" placeholder="Ingrese Usuario">
							</div>
							<div class="form-group">
								<input type="password" class="form-control form-control-user" id="txtPassword" name="txtPassword" placeholder="Ingrese su contraseña">
							</div>
							<br>
							<div class="form-group">
								<button class="btn btn-primary btn-block btn-user" id="btnIngreso"> Ingreso </button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>