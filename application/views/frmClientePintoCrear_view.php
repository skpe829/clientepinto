<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Clientes</h1>
    <p>Clientes que cumplen las condiciones para ser Socios Pinto</p>


    <a href="#" class="btn btn-success btn-icon-split" id="btn_agregarClientes">
        <span class="icon text-white-50">
            <i class="fas fa-check"></i>
        </span>
        <span class="text"> Agregar Todos </span>
    </a>

    <div class="my-2"></div>

    <div class="card mb-4">
        <form id="frmCliente">
        	<table class="table table-hover table-condensed" id="tblCliente">
        		<thead>
        			<th>Cedula</th>
        			<th>Nombre</th>
        			<th>Correo</th>
                    <th>Telefono</th>
                    <th>Telefono 2</th>
        			<th>Sexo</th>
        			<th>Fecha Nacimiento</th>
        			<th>Ult. Almacen</th>
        		</thead>
        		<tbody></tbody>
        	</table>
        </form>
    </div>
</div>