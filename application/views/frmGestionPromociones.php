<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Promociones</h1>
    <p>Promociones para la creacion de Clientes Pinto</p>


    <div class="card mb-4">
        <div class="p-4">
            <h5>Nueva Promocion:</h5>
            <div class="form-group row">
                <div class="col-sm-4 mb-4 mb-sm-0">
                    <select class="form-control form-control-user" id="sltAlmacenes" name="sltAlmacenes">
                        <option disabled="" selected=""> -- Seleccione un almacen -- </option>
                        <option value='00'> TODOS LOS ALMACENES </option>
                    </select>
                </div>
                <div class="col-sm-8 mb-8 mb-sm-0"><input type="text" class="form-control form-control-user" id="txt_NombrePromo" placeholder="Nombre de la Promocion"></div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3 mb-2 mb-sm-0"><input type="number" class="form-control form-control-user" id="txt_CantFact" placeholder="Cantidad Facturas"></div>
                <div class="col-sm-3 mb-2 mb-sm-0"><input type="number" class="form-control form-control-user" id="txt_MontFact" placeholder="Facutrado (IVA Incluido)"></div>
                <div class="col-sm-3 mb-2 mb-sm-0"><input type="text" class="form-control form-control-user" id="txt_FecIni" placeholder="Fecha Inicio:"></div>
                <div class="col-sm-3 mb-2 mb-sm-0"><input type="text" class="form-control form-control-user" id="txt_FecFin" placeholder="Fecha Fin:"></div>
            </div>

            <button class="btn btn-success"> Agregar Promo </button>
            <button class="btn btn-danger"> Borrar Campos </button>
        </div>
    </div>

    <hr>

    <div class="card mb-4">
        <form id="frmCliente">
        	<table class="table table-hover table-condensed" id="tblCliente">
        		<thead>
        			<th>Opt</th>
        			<th>Nombre Promo</th>
        			<th>Cant. Fact</th>
                    <th>Monto Fact</th>
                    <th>Fecha Ini</th>
        			<th>Fecha Fin</th>
        		</thead>
        		<tbody></tbody>
        	</table>
        </form>
    </div>
</div>