			</div>
			<!-- End of Main Content -->

	    </div>
	    <!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

</body>
    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo base_url('vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <!-- Core plugin JavaScript-->
    <script src="<?php echo base_url('vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url('vendor/js/sb-admin-2.min.js'); ?>"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url('vendor/sweetalert/sweetalert.min.js'); ?>"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url('vendor/dataTables/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('vendor/dataTables/dataTables.bootstrap4.min.js'); ?>"></script>

    <script src="<?php echo base_url('vendor/datepicker/jquery-ui.min.js'); ?>"></script>
    <script src="<?php echo base_url('vendor/datepicker/jquery-ui-es.js'); ?>"></script>


    

	<!-- Custom JavaScript -->
	<?php
	if(isset($js_load)){
		foreach($js_load as $value) {
	?>
		<script type="text/javascript" src="<?php echo base_url("vendor/js/".$value.'?'.rand()); ?>"></script>
	<?php
		}
	}
	?>
</html>