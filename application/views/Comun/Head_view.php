<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Clientes Pinto - Login</title>
    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url('vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="<?php echo base_url('vendor/css/sb-admin-2.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('vendor/datatables/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('vendor/datepicker/jquery-ui.min.css'); ?>" rel="stylesheet" type="text/css">

    <!-- Spinner -->
    <style type="text/css">
        .wait_spin {
            display:    none;
            position:   fixed;
            z-index:    100001;
            top:        0;
            left:       0;
            height:     100%;
            width:      100%;
            background: rgba( 255, 255, 255, .8 ) 
                        url(<?php echo base_url('vendor/images/pIkfp.gif') ?>) 
                        50% 50% 
                        no-repeat;
        }
    </style>
    <script>var base_url = '<?php echo base_url() ?>';</script>
</head>
<body style="background-image: url(<?php echo base_url('vendor/images/fondo.png') ?>);" id="page-top">
    <div class="wait_spin" id="wait"></div>

    <!-- Page Wrapper -->
    <div id="wrapper">