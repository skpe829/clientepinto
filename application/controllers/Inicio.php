<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {


	public function index()
	{
		$this->load->view('Comun/Head_view');
		$this->load->view('Inicio_view');
		$data['js_load'] = array("login.js");
		$this->load->view('Comun/Footer_view',$data);
	}

	public function Login(){
		$this->load->model('Usuarios_model');
		$data = array($this->input->post('txtUsuario'),$this->input->post('txtPassword'));
		$user = $this->Usuarios_model->getUsuario($data);
		if($user->num_rows()==1){
			$usuario = $user->Row();

			$usuario_data = array(
			'cod_usr' => $usuario->ced_ruc,
			'email' => TRIM($usuario->email),
			'nombre' => TRIM($usuario->nombre),
			'user_name' => TRIM($usuario->username),
			'privilegios' => TRIM($usuario->permiso),
			'login' => TRUE);
			
			$this->session->set_userdata($usuario_data);

			if($usuario->permiso == 3 OR $usuario->permiso == 2){
				$this->load->model('Almacen_model');
				$ip = explode('.',gethostbyname(gethostbyaddr($_SERVER['REMOTE_ADDR'])));
				//$array = array($ip[2],$ip[3]);
				$array = array(8,15);
				$almacen = $this->Almacen_model->getAlmacenByIp($array);
				
				if($almacen->num_rows()>0){
					$this->session->set_userdata('cod_alm', $almacen->Row()->cod_alm);
					echo "<script> swal('info','Redireccionando...','info');window.location.href = '".base_url('Inicio/Inicio_Usuario')."'; </script>";
				}else{
					echo "<script> swal('Eh no...','Esta cuenta es para uso unico en almacenes...','warning');</script>";
				}
			}else{
				$this->session->set_userdata('cod_alm', '00');
				echo "<script> swal('info','Redireccionando...','info');window.location.href = '".base_url('Inicio/Inicio_Usuario')."'; </script>";
			}
		}else{
			echo "<script> swal('Eh no...','Usuario/Contraseña invalidos','error');</script>";
		}
	}

	public function Inicio_Usuario(){
		if($this->session->userdata('login')){
			$this->load->view('Comun/Head_view');
			$this->load->view('Comun/Menu_view');
			$this->load->view('Comun/Footer_view');
		}else{
			Redirect("/Usuarios/desconectar");			
		}
	}




}
