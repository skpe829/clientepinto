<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Pinto extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Almacen_model');
		$this->load->model('Clientes_model');
	}


	public function index(){
		if($this->session->userdata('login')){
			$this->load->view('Comun/Head_view');
			$this->load->view('Comun/Menu_view');
			$data['js_load'] = array('clientes.js');
			$this->load->view('Comun/Footer_view',$data);
		}else{
			Redirect("/Usuarios/desconectar");			
		}
	}

	public function ConsultarClientes(){
		if($this->session->userdata('login')){
			$this->load->view('Comun/Head_view');
			$this->load->view('Comun/Menu_view');
			$this->load->view('frmClientePintoCrear_view');
			$data['js_load'] = array('clientes.js');
			$this->load->view('Comun/Footer_view',$data);
		}else{
			Redirect("/Usuarios/desconectar");			
		}
	}

	public function getClientesCalificados(){
		if($this->session->userdata('login')){
			$clientes = $this->Clientes_model->getClientesPinto($this->session->userdata('cod_alm'));

			foreach ($clientes->Result() as $key => $value) {
				$infoCliente = $this->Clientes_model->getInfoClientes($value->ced_ruc);

				$clienteCompleto[$value->ced_ruc]['cedula'] = $value->ced_ruc;
				$clienteCompleto[$value->ced_ruc]['nombres'] = utf8_encode($infoCliente->nombres);
				$clienteCompleto[$value->ced_ruc]['email'] = utf8_encode($infoCliente->email);
				$clienteCompleto[$value->ced_ruc]['sexo'] = $infoCliente->sexo;
				$clienteCompleto[$value->ced_ruc]['almacen'] = utf8_encode($value->nom_alm);
				$clienteCompleto[$value->ced_ruc]['fec_nac'] = $infoCliente->fec_nac;
				$clienteCompleto[$value->ced_ruc]['telef'] = $infoCliente->telef;
				$clienteCompleto[$value->ced_ruc]['telef2'] = $infoCliente->telef2;
				$clienteCompleto[$value->ced_ruc]['cod_alm'] = $value->cod_alm;
				$clienteCompleto[$value->ced_ruc]['dir'] = $infoCliente->dir;
			}

			echo json_encode($clienteCompleto);

		}else{
			Redirect("/Usuarios/desconectar");			
		}
	}

	public function setClientesPinto(){
		$clientes = $this->input->post('clientesPinto');
		$cPinto = $this->Clientes_model->setClientesPinto($clientes);

		echo json_encode($cPinto);
			
	}
	/*public function get(){
		$promo = $this->Almacen_model->getPromoAlmacen($this->session->user_data('cod_alm'));
		$prm['mnt_min'] = $promo->mnt_min; 
		$prm['rng_dia'] = $promo->rng_dia;
		$prm['cnt_fct'] = $promo->cnt_fct;
	}*/

}

?>