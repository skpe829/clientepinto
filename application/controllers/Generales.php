<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generales extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Almacen_model');
	}


	public function getAlmacenes() {
	/*	$almacenes = $this->Almacen_model->getAlmacenes();
		foreach ($almacenes as $key => $value) {
			$rs[]['cod_alm'] = $almacenes->cod_alm;
			$rs[]['nom_alm'] = utf8_encode($almacenes['nom_alm']);
		}
*/
		echo json_encode($this->Almacen_model->getAlmacenes()->Result_array());
	}
}

?>