<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promocion extends CI_Controller {


	public function PromosAlmacenes() {
		if($this->session->userdata('login')){
			$this->load->view('Comun/Head_view');
			$this->load->view('Comun/Menu_view');
			$this->load->view('frmGestionPromociones');
			$data['js_load'] = array('promociones.js');
			$this->load->view('Comun/Footer_view',$data);
		}else{
			Redirect("/Usuarios/desconectar");			
		}
	}
}

?>